const field = document.querySelector(".field");
const cellsCount = 30 * 30;

for (let i = 0; i < cellsCount; i++) {
    const cell = document.createElement("div");
    cell.style.cssText = `
        width: 20px;
        height: 20px;
        border: 1px solid black;
    `;

    field.append(cell);
}

field.addEventListener("click", function (e) {
    e.stopPropagation();
    if (e.target.tagName !== "DIV") {
        return;
    }

    e.target.classList.toggle("div-inverse");
});

document.body.addEventListener("click", function (e) {
    if (field.classList.contains("table-normal")) {
        field.classList.replace("table-normal", "table-inverse");
    } else {
        field.classList.replace("table-inverse", "table-normal");
    }
});
